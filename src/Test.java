import it.owldev.htmlNode.Tag;
import it.owldev.htmlNode.attribute.Attribute;

import java.util.ArrayList;

public class Test {

    public static void main(String[] args) {
        ArrayList<Tag> tags = new ArrayList<Tag>();
        ArrayList<Attribute> attributes = new ArrayList<Attribute>();
        Tag tag = new Tag("html", tags, attributes);
        Tag head = new Tag("head");
        tag.addTag(head);
        ArrayList<Tag> tagsBody = new ArrayList<Tag>();
        ArrayList<Attribute> attributesBody = new ArrayList<Attribute>();
        Tag body = new Tag("body", tagsBody, attributesBody);
        tag.addTag(body);
        head.addAttribute(new Attribute("style", "display: none;"));
        System.out.println(tag);
    }
}
