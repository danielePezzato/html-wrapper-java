package it.owldev.htmlNode.attribute;

public class Attribute implements AttributeInterface
{
    private String name;
    private String value;

    public Attribute(String name, String value)
    {
        this.name = name;
        this.value = value;
    }

    public String render()
    {
        return this.name + "=\"" + this.value + "\"";
    }
}
