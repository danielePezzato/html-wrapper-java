package it.owldev.htmlNode;

import it.owldev.htmlNode.attribute.Attribute;

import java.util.ArrayList;

public class Tag implements TagInterface
{
    private String name;
    private ArrayList<Tag> tags;
    private ArrayList<Attribute> attributes;
    private boolean closingTag;


    public Tag(String name )
    {
        this.name = name;
        this.tags = new ArrayList<Tag>();
        this.attributes = new ArrayList<Attribute>();
        this.closingTag = true;
    }

    public Tag(String name, ArrayList<Attribute> attributes )
    {
        this.name = name;
        this.tags = new ArrayList<Tag>();
        this.attributes = attributes;
        this.closingTag = true;
    }

    public Tag(String name, ArrayList<Tag> tags, ArrayList<Attribute> attributes)
    {
        this.name = name;
        this.tags = tags;
        this.attributes = attributes;
        this.closingTag = true;
    }

    public Tag(String name, ArrayList<Tag> tags, ArrayList<Attribute> attributes, boolean closingTag)
    {
        this.name = name;
        this.tags = tags;
        this.attributes = attributes;
        this.closingTag = closingTag;
    }

    public Tag addTag(Tag tag)
    {
        this.tags.add(tag);
        return this;
    }

    public Tag addAttribute(Attribute attribute)
    {
        this.attributes.add(attribute);
        return this;
    }

    public String render()
    {
        String toString = "<" + this.name;
        if(!this.attributes.isEmpty()) {
            for (Attribute attribute:this.attributes) {
                toString += " " + attribute.render() + " ";
            }
        }
        if(!this.tags.isEmpty()) {
            toString += ">";
            for (Tag tag:this.tags) {
                toString += " " + tag.render() + " ";
            }
            toString += "</" + this.name + ">";
        } else {
            if(this.closingTag) {
                toString += "></" + this.name + ">";
            } else {
                toString += "/>";
            }
        }
        return toString;
    }

    public String toString(){
        return this.render();
    }
}
